package com.gustibimo.learnspring.catalog.dao;

import com.gustibimo.learnspring.catalog.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {
}
