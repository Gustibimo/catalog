package com.gustibimo.learnspring.catalog.controller;


import com.gustibimo.learnspring.catalog.dao.ProductDao;
import com.gustibimo.learnspring.catalog.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


//s
@RestController @RequestMapping("/api/product")
public class ProductApiController {
    @Autowired private ProductDao productDao;

    @GetMapping("/")
    public Page<Product> findProducts(Pageable page){
        return productDao.findAll(page);
    }
}
